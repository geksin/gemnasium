require "tmpdir"

require_relative './test_project_scan.rb'
require_relative './support/shared_examples/scan_shared_examples.rb'
require_relative './support/shared_examples/report_shared_examples.rb'

def analyzer_image_name
  ENV.fetch("TMP_IMAGE", "gemnasium:latest")
end

TestProjectScan.tap do |config|
  config.image_name = analyzer_image_name
  config.fixtures_dir = ENV.fetch("FIXTURES_DIR", Dir.tmpdir)
  config.test_projects_url = "https://gitlab.com/gitlab-org/security-products/tests"
  config.global_vars = {"GEMNASIUM_DB_REF_NAME": "v1.2.142"}
  config.report_filename = "gl-dependency-scanning-report.json"
end

describe "running image" do
  context "with no project" do
    before(:context) do
      @output = %x{docker run -t --rm -w /app #{analyzer_image_name}}
      @exit_code = $?.to_i
    end

    it "outputs analyzer version" do
      analyzer_version = "2.29.7"
      expect(@output).to match(/GitLab Gemnasium analyzer v#{analyzer_version}/i)
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify{ expect(@exit_code).to eql 0 }
    end
  end

  context "with test project" do
    context "go-modules" do
      context "master branch" do
        before(:context) do
          @report_path, @report, @exit_code = TestProjectScan.new(
            "go-modules"
          ).run
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report", ["go.sum"]
          it_behaves_like "recorded report", "go-modules"
          it_behaves_like "valid report"
        end
      end

      context "when excluding go.sum with DS_EXCLUDED_PATHS" do
        before(:context) do
          @report_path, @report, @exit_code = TestProjectScan.new(
            "go-modules",
            variables: { "DS_EXCLUDED_PATHS": "/go.sum" },
          ).run
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "empty report"
          it_behaves_like "valid report"
        end
      end

      context "when moving go.mod, go.mod to sub-directory" do
        before(:context) do
          move_to_subdir = Proc.new do |project_dir|
            %x{
            mkdir -p #{project_dir}/subdir
            git -C #{project_dir} mv go.mod subdir/go.mod
            git -C #{project_dir} mv go.sum subdir/go.sum
            }
          end

          @report_path, @report, @exit_code = TestProjectScan.new(
            "go-modules",
            mutate_project: move_to_subdir
          ).run
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report", ["subdir/go.sum"]
          it_behaves_like "valid report"
        end
      end
    end

    context "php-composer" do
      context "with broken composer.lock" do
        before(:context) do
          write_to_lockfile = Proc.new do |project_dir|
            %x{
            echo "broken JSON" > #{project_dir}/composer.lock
            }
          end

          @report_path, @report, @exit_code = TestProjectScan.new(
            "php-composer",
            mutate_project: write_to_lockfile
          ).run
        end

        it_behaves_like "failed scan"
      end
    end

    context "ruby-bundler" do
      context "master branch" do
        before(:context) do
          @report_path, @report, @exit_code = TestProjectScan.new(
            "ruby-bundler"
          ).run
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report", ["Gemfile.lock"]
          it_behaves_like "recorded report", "ruby-bundler"
          it_behaves_like "valid report"
        end
      end

      context "when renaming Gemfile.lock to gems.locked" do
        before(:context) do
          rename_dep_files = Proc.new do |project_dir|
            %x{
            git -C #{project_dir} mv Gemfile gems.rb
            git -C #{project_dir} mv Gemfile.lock gems.locked
            }
          end

          @report_path, @report, @exit_code = TestProjectScan.new(
            "ruby-bundler",
            mutate_project: rename_dep_files
          ).run
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report", ["gems.locked"]
          it_behaves_like "valid report"
        end
      end
    end
  end
end
