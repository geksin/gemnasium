package scanner

import (
	"encoding/json"
	"flag"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/go"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	vrange "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"
)

func init() {
	// register version range resolvers
	for _, syntax := range []string{"npm", "gem", "python"} {
		vrange.Register(syntax, "../vrange/semver/vrange-"+runtime.GOOS, "npm")
	}
}

func TestScanner(t *testing.T) {

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db", "doc")
	set.Bool("gemnasium-db-update-disabled", true, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	require.NoError(t, err)

	t.Run("ScanProjects", func(t *testing.T) {
		dir := "testdata/depfiles"
		files := []string{
			"pypi/pipdeptree.json",
			"go/go.sum",
			"yarn/yarn.lock",
			"bundler/Gemfile.lock",
		}
		projects := []finder.Project{}
		for _, path := range files {
			p := finder.Project{Dir: filepath.Dir(path)}
			p.AddScannableFilename(filepath.Base(path))
			projects = append(projects, p)
		}
		got, err := scanner.ScanProjects(dir, projects)
		require.NoError(t, err)

		// ignore dependencies because pointers cannot be compared
		for i := range got {
			got[i].Dependencies = nil
		}

		want := []File{}
		sort.Slice(got, func(i, j int) bool {
			return got[j].Path > got[i].Path
		})
		checkExpectedFile(t, &want, &got, filepath.Join(dir, "scandir.json"))
	})

	t.Run("ScanFile", func(t *testing.T) {
		t.Run("Gemfile.lock", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "bundler/Gemfile.lock"
			path := filepath.Join(dir, name)
			alias := "bundler/Gemfile"
			got, err := scanner.ScanFile(path, alias)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-gem.json"))
		})

		t.Run("pipdeptree.json", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "pypi/pipdeptree.json"
			path := filepath.Join(dir, name)
			alias := "pypi/pipdeptree.json"
			got, err := scanner.ScanFile(path, alias)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-pypi.json"))
		})

		t.Run("go.sum", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "go/go.sum"
			path := filepath.Join(dir, name)
			alias := "go/go.sum"
			got, err := scanner.ScanFile(path, alias)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-go.json"))
		})
	})
}

func TestScanner_GemOnlyRepo(t *testing.T) {

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db-gem", "doc")
	set.Bool("gemnasium-db-update-disabled", true, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	require.NoError(t, err)

	var tcs = []struct {
		file    string
		wantErr error
	}{
		{
			file:    "bundler/Gemfile.lock",
			wantErr: nil,
		},
		{
			file:    "pypi/pipdeptree.json",
			wantErr: advisory.ErrNoAdvisoryForPackageType{PackageType: "pypi"},
		},
		{
			file:    "yarn/yarn.lock",
			wantErr: advisory.ErrNoPackageTypeDir{PackageType: "npm"},
		},
	}

	dir := "testdata/depfiles"
	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			path := filepath.Join(dir, tc.file)
			_, err := scanner.ScanFile(path, tc.file)
			require.Equal(t, tc.wantErr, err)
		})
	}
}

func checkExpectedFile(t *testing.T, want, got interface{}, path string) {
	// look for file containing expected JSON document
	if _, err := os.Stat(path); err != nil {
		createExpectedFile(t, got, path)
	} else {
		compareToExpectedFile(t, want, got, path)
	}
}

func compareToExpectedFile(t *testing.T, want, got interface{}, path string) {
	f, err := os.Open(path)
	require.NoError(t, err)
	defer f.Close()

	err = json.NewDecoder(f).Decode(want)
	require.NoError(t, err)

	require.Equal(t, want, got)
}

func createExpectedFile(t *testing.T, got interface{}, path string) {
	// make test fail
	t.Errorf("creating JSON document: %s", path)

	// create directory for file
	err := os.MkdirAll(filepath.Dir(path), 0755)
	require.NoError(t, err)

	// create file
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	require.NoError(t, err)
	defer f.Close()

	// write JSON doc
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")
	encoder.SetEscapeHTML(false)
	err = encoder.Encode(got)
	require.NoError(t, err)
}
