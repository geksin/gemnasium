package gosum

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestParse(t *testing.T) {
	types := []string{"small", "big", "duplicates", "malformed", "incompatible"}
	for _, testType := range types {
		t.Run(testType, func(t *testing.T) {
			b, err := ioutil.ReadFile("expect/" + testType + "/packages.json")
			require.NoError(t, err)
			want := []parser.Package{}
			err = json.Unmarshal(b, &want)
			require.NoError(t, err)

			text, err := os.Open("fixtures/" + testType + "/go.sum")
			require.NoError(t, err)

			got, _, err := Parse(text)
			require.NoError(t, err)

			require.ElementsMatch(t, want, got)
		})
	}
}
